import os

from django.conf import settings
from django.contrib import admin
from django.urls import include
from django.urls import path
from django.urls import re_path
from django.views.static import serve

from trap.views import MessagePage, StaticPage

urlpatterns = [
    path('admin/', admin.site.urls),
    path('<slug:channel>/<int:pk>/', MessagePage.as_view(), name='message'),
    path('summernote/', include('django_summernote.urls')),
    re_path(r'^(?P<path>favicon.ico)$', serve, {'document_root': os.path.join(settings.BASE_DIR, 'trap', 'static')}),
    re_path(r'^(?P<slug>.*)$', StaticPage.as_view(), name='page'),
]
