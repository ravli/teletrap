from django.views.generic import DetailView

from trap.models import Message, Page


class MessagePage(DetailView):
    model = Message
    template_name = 'message_page.html'
    
    channel = None
    tgid = None
    
    def dispatch(self, request, *args, **kwargs):
        self.channel = kwargs["channel"]
        self.tgid = kwargs["pk"]
    
        return super(MessagePage, self).dispatch(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        return Message.objects.get(channel__username=self.channel, tgid=self.tgid)
    

class StaticPage(DetailView):
    model = Page
    template_name = 'static_page.html'
