import datetime
from django.db import models
from django.urls import reverse


class Channel(models.Model):
    tgid = models.CharField(max_length=255, db_index=True)
    name = models.CharField(max_length=255, db_index=True)
    username = models.CharField(max_length=255, unique=True)
    
    class Meta:
        unique_together = [('tgid', 'username')]
    
    def __str__(self):
        return self.name
    

class Message(models.Model):
    tgid = models.CharField(max_length=255, db_index=True)
    hash = models.CharField(max_length=255, db_index=True)
    channel = models.ForeignKey(Channel, on_delete=models.SET_NULL, null=True)
    timestamp = models.DateTimeField()
    text = models.TextField()
    html = models.TextField(default="")
    raw_message = models.TextField(default="")
    last_accessed = models.DateTimeField(default=datetime.datetime.now)

    class Meta:
        unique_together = [('tgid', 'channel')]

    def get_absolute_url(self):
        return reverse('message', args=[self.channel.username, self.tgid])
    

class Page(models.Model):
    slug = models.SlugField(max_length=255, db_index=True, blank=True, unique=True)
    title = models.CharField(max_length=255)
    html = models.TextField()

    def __str__(self):
        return self.title

