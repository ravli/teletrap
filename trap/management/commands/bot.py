# coding: utf-8

import datetime
import hashlib
import os
import time

from django.conf import settings
from django.core.management.base import BaseCommand

import markdown
from telegram.ext import Filters, Updater, MessageHandler, CommandHandler
import rollbar

from trap.models import Message, Channel


class Command(BaseCommand):
    help = 'Runs tg bot'

    def handle(self, *args, **options):
        if os.environ.get("DATABASE_URL"):
            # We're on heroku, let's wait while it brings down other instances
            time.sleep(120)
        
        try:
            print("Starting the bot...")
            
            updater = Updater(token=settings.TG_BOT_TOKEN)
            dispatcher = updater.dispatcher
    
            start_handler = CommandHandler('start', self.start)
            dispatcher.add_handler(start_handler)
            
            msg_handler = MessageHandler(Filters.all, self.handle_message)
            dispatcher.add_handler(msg_handler)
            
            updater.start_polling()
        except Exception as e:
            print(e)
            if rollbar.SETTINGS.get('access_token'):
                rollbar.report_message(e)
                rollbar.report_exc_info()

    def start(self, bot, update):
        um = update.message
        bot.send_message(chat_id=um.chat.id, text="Отфорварди мне пост из телеграм-канала, и я дам тебе ссылку для "
                                                  "обсуждения его в www!")

    def handle_message(self, bot, update):
        um = update.message
        try:
            if not um.forward_from_chat or um.forward_from_chat.type != "channel":
                print("Not a channel!")
                bot.send_message(chat_id=um.chat.id, text="Я работаю только с постами из каналов")
                return

            chat_id = um.forward_from_chat.id
            chat_name = um.forward_from_chat.title
            chat_username = um.forward_from_chat.username
            msg_id = um.forward_from_message_id
            
            if isinstance(um.date, datetime.datetime):
                msg_timestamp = um.date
            else:
                msg_timestamp = datetime.datetime.fromtimestamp(int(um.date))
    
            if um.text:
                msg_text = um.text
                msg_hash = hashlib.md5(msg_text.encode('utf-8')).hexdigest()
                msg_html = "<script async src='https://telegram.org/js/telegram-widget.js?4' data-telegram-post='%s/%s' data-width='100%%'></script>" % (
                    chat_username,
                    msg_id,
                )
            elif um.photo:
                largest_photo = sorted(um.photo, key=lambda p: p["file_size"])[-1]
                msg_hash = largest_photo.file_id
                msg_text = "[image:%s]" % largest_photo.file_id
                msg_html = "<script async src='https://telegram.org/js/telegram-widget.js?4' data-telegram-post='%s/%s' data-width='100%%'></script>" % (
                    chat_username,
                    msg_id,
                )
            elif um.video:
                msg_hash = um.video.thumb.file_id
                msg_text = "[video:%s:%s]" % (um.video.file_id, um.video.thumb.file_id)
                msg_html = "<script async src='https://telegram.org/js/telegram-widget.js?4' data-telegram-post='%s/%s' data-width='100%%'></script>" % (
                    chat_username,
                    msg_id,
                )
            elif um.document:
                msg_hash = um.document.thumb.file_id
                msg_text = "[document:%s:%s]" % (um.document.file_id, um.document.thumb.file_id)
                msg_html = "<script async src='https://telegram.org/js/telegram-widget.js?4' data-telegram-post='%s/%s' data-width='100%%'></script>" % (
                    chat_username,
                    msg_id,
                )
            else:
                if rollbar.SETTINGS.get('access_token'):
                    rollbar.report_message("Not valid data: %s" % update)
                bot.send_message(chat_id=um.chat.id, text="Сорян, я пока работаю только с текстовыми, видео- и фото-постами.")
                return

            try:
                chat = Channel.objects.get(username=chat_username)
            except Channel.DoesNotExist:
                chat = Channel.objects.create(tgid=chat_id, name=chat_name, username=chat_username)

            try:
                msg = Message.objects.get(channel=chat, tgid=msg_id)
            except Message.DoesNotExist:
                msg = Message.objects.create(channel=chat, tgid=msg_id, hash=msg_hash, timestamp=msg_timestamp,
                                             text=msg_text, html=msg_html, raw_message="%s" % update)
            else:
                msg.last_accessed = datetime.datetime.now()
                msg.save()
    
            reply = "Обсуждаем: %s%s" % (settings.HOST_NAME, msg.get_absolute_url())
            bot.send_message(chat_id=um.chat.id, text=reply)
        except Exception as e:
            print(e)
            if rollbar.SETTINGS.get('access_token'):
                rollbar.report_message(e)
                rollbar.report_exc_info()
