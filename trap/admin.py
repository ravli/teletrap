from django.conf import settings
from django.contrib import admin
from django.utils.html import format_html
from django_summernote.admin import SummernoteModelAdmin

from trap.models import Channel, Message, Page

admin.site.register(Channel)


class MessageAdmin(admin.ModelAdmin):
    list_display = ['channel', 'timestamp', 'get_url']
    
    def get_url(self, instance):
        url = "%s%s" % (settings.HOST_NAME, instance.get_absolute_url())
        return format_html("<a href='{}'>{}</a>", url, url)
    
    get_url.short_description = 'URL'

admin.site.register(Message, MessageAdmin)


class StaticPageAdmin(SummernoteModelAdmin):
    summernote_fields = ('html',)
    list_display = ['title', 'slug']
    
    def has_add_permission(self, request):
        return False
    
    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Page, StaticPageAdmin)
