import datetime
import json
import os

from django.test import Client
from django.test import TestCase

from trap.management.commands.bot import Command
from trap.models import Channel, Message

from telegram import Update


class WebTestCase(TestCase):
    def test_message(self):
        channel = Channel.objects.create(tgid="ID123", name="channel 1", username="ch1")
        msg = Message.objects.create(
                tgid="1235",
                hash="123",
                channel=channel,
                timestamp=datetime.datetime.now(),
                text="text\nlines",
                html="text<br/>lines",
                raw_message="{}"
        )
        client = Client()
        r = client.get(msg.get_absolute_url())
        self.assertEqual(r.status_code, 200)
        self.assertContains(r, "text<br/>lines")

    def test_favicon(self):
        client = Client()
        r = client.get('/favicon.ico')
        self.assertEqual(r.status_code, 200)

    def test_home(self):
        client = Client()
        r = client.get('/')
        self.assertEqual(r.status_code, 200)
        self.assertContains(r, "руддщ цщкдв")


class MockBot(object):
    chat_id = None
    text = None
    
    def send_message(self, chat_id, text):
        self.chat_id = chat_id
        self.text = text


class BotTestCase(TestCase):
    def test_image(self):
        img_data = json.load(open(os.path.join(os.path.dirname(__file__), "./test_data/img.json")))
        cmd = Command()
        bot = MockBot()
        tgm = Update.de_json(img_data, bot)
        cmd.handle_message(bot, tgm)
        
        self.assertEqual(bot.chat_id, 136333084)
        self.assertEqual(bot.text, "Обсуждаем: http://127.0.0.1:8000/apazhannel/881/")
        self.assertEqual(Message.objects.get(channel__username="apazhannel", tgid="881").html,
                         "<script async src='https://telegram.org/js/telegram-widget.js?4' "
                         "data-telegram-post='apazhannel/881' data-width='100%'></script>")

    def test_video(self):
        video_data = json.load(open(os.path.join(os.path.dirname(__file__), "./test_data/video.json")))
        cmd = Command()
        bot = MockBot()
        tgm = Update.de_json(video_data, bot)
        cmd.handle_message(bot, tgm)
        
        self.assertEqual(bot.chat_id, 136333084)
        self.assertEqual(bot.text, "Обсуждаем: http://127.0.0.1:8000/apazhannel/876/")
        
        self.assertEqual(Message.objects.get(channel__username="apazhannel", tgid="876").html,
                         "<script async src='https://telegram.org/js/telegram-widget.js?4' "
                         "data-telegram-post='apazhannel/876' data-width='100%'></script>")